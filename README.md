# EasyTemplate [![Gem Version](https://badge.fury.io/rb/easy_template.svg)](https://badge.fury.io/rb/easy_template)

Easy template language in ruby, with only variable substitution.

## Documentation

```js
/**
 * @param {string} text - the string containing your template.
 * @param {object} variables - containing the values to substitute in place of the keys enclosed by `{` and `}`.
 */
EasyTemplate.process(text, variables)
```
NB: If you want to use raw `{` in your template you can escape it with `\`.

## Examples:

```js
EasyTemplate.process('Hello Mr {last name}!', {
    'last name': 'Bowie'
  })
```
_Hello Mr Bowie!_

```js
EasyTemplate.process('\{first name} {last name}!', {
    'first name': 'Nina',
    'last name':  'Simone'
  })
```
_{first name} Simone!_

## License

Copyright © 2019 Nathan Gouy, EasyTemplate is free software, and may be redistributed under the terms specified in the [license](LICENSE).
const EasyTemplate = require('../distribution/easy-template');

describe('test_empty_string', () => {
  it('', () => expect(EasyTemplate.process('', {})).toEqual(''))
});

describe('test_without_substitution', () => {
  it('', () => expect(EasyTemplate.process('test', {})).toEqual('test'))
});

describe('test_with_a_single_substitution', () => {
  it('', () => expect(EasyTemplate.process('{hello}', {'hello': 'world!'})).toEqual('world!'))
});

describe('test_with_static_text_and_substitution', () => {
  it('', () => expect(EasyTemplate.process('Hello Mr {last name}!', {'last name': 'Bowie'})).toEqual('Hello Mr Bowie!'))
});

describe('test_with_multiple_substitutions', () => {
  it('', () => expect(EasyTemplate.process('Hello Mrs {first name} {last name}! {word} {word}', {'first name': 'Nina', 'last name': 'Simone', 'word': 'bye'})).toEqual('Hello Mrs Nina Simone! bye bye'))
});

describe('test_with_escaped_text', () => {
  it('', () => expect(EasyTemplate.process('Hello Mrs \\{first name} {last name}!', {'first name': 'Nina', 'last name': 'Simone'})).toEqual('Hello Mrs {first name} Simone!'))
});

describe('test_with_escaped_text_at_begin', () => {
  it('', () => expect(EasyTemplate.process('\\{first name} {last name}!', {'first name': 'Nina', 'last name': 'Simone'})).toEqual('{first name} Simone!'))
});

describe('test_with_escaped_text_everywhere', () => {
  it('', () => expect(EasyTemplate.process('\\{first nam\{e} \\{{last name}\\{', {'first name': 'Nina', 'last name': 'Simone'})).toEqual('{first nam{e} {Simone{'))
});

describe('test_fun', () => {
  it('', () => expect(EasyTemplate.process('{fun }{{{}}} {test yeah!}', {'fun }{{{}}} {test yeah!': 'Not bad ={D!'})).toEqual('Not bad ={D!'))
});